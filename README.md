# FoursquareApp

This sample has following implementations:

1. The main screen should display a search input, and should use industry best practices to perform a typeahead search against the Foursquare API.
1. When a search returns results, these should be displayed in a list format. Each list item should provide, at a minimum, the name of the place (e.g., Flitch Coffee), the category of the place (e.g., Coffee Shop), the icon from the response, the distance from the center of Seattle (47.6062° N, 122.3321° W) to the place. Clicking a list item should launch the details screen for that place.
1. The details screen for a place should use a collapsible toolbar layout to show a map in the upper half of the screen, with two pins -- one, the location of search result, and the other, the center of Seattle. The bottom half of the details screen should provide details about the place.
1. The user should be able to navigate between screens according to Android platform conventions.
1. Include instructions for building the application and any relevant documentation in a README.md file
1. Please post your submission on Github, Bitbucket or Gitlab.

Also I have implemented following optional requirements:

1. In search results, each list item should also indicate whether the place has been favorited by the user. User should be able to favorite the place from details screen.
1. Favorite selections should be changeable (i.e., you can favorite/unfavorite a place), should persist across launches of the app, and should show correctly on both the search result list and details screens.

In this sample I have used following things:

1. Kotlin
1. Full MVVM Architecture
1. Room for persistence 
1. LiveData
1. Google Maps Static API
1. Foursqare API
1. Collapsable Toolbar
1. Androidx

NEW ADDITIONS 20-06-19

1. Screen Rotate EditText fix
1. Bugfix in search - causing favourites to disappear 
1. Details screen tidy up
1. onDestroy Memory leak fixes
1. ViewModel selected item singleton architecture cleanup
