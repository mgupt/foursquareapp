package com.example.foursquaresample

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), SearchFragment.OnSearchFragmentInteractionListener,
    DetailsFragment.DetailsFragmentInteractionListener {


    companion object {
        val TRIGGER_SERACH = 100
        val SEARCH_TRIGGER_DELAY_IN_MS = 300L
        val SEARCH_TAG = "search_tag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()


        makeAppBarLayoutCollapsed()
        val fragment = supportFragmentManager.findFragmentByTag(SEARCH_TAG)
        if (fragment == null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.container, SearchFragment.newInstance(), SEARCH_TAG)
            ft.commit()
        }

        // For solving the empty room problem - so that we have some dat on screen initially to play with
        val mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        mainActivityViewModel.getPlaces("coffee")
    }

    private fun initToolbar() {
        toolbar_layout.setExpandedTitleColor(Color.parseColor("#00FFFFFF"))
        toolbar_layout.setCollapsedTitleTextColor(Color.parseColor("#FFFFFF"))
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            makeAppBarLayoutCollapsed()
        }
        super.onBackPressed()
    }

    override fun onItemClick(position: Int) {
        makeAppBarLayoutExpandable()
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.container, DetailsFragment.newInstance())
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun loadMap(lat: Double, lng: Double) {
        val sb = StringBuilder()
        sb.append("https://maps.googleapis.com/maps/api/staticmap?size=600x430&maptype=roadmap&markers=color:blue%7Clabel:C%7C47.60621,-122.33207&markers=color:red%7Clabel:P%7C")
            .append(lat.toString())
            .append(",")
            .append(lng.toString())
            .append("&key=AIzaSyBa7JKNniLbtXku3mR2pqfv05zWvCO6_cE")
        Glide.with(this).load(sb.toString()).into(toolbar_image)
    }

    fun makeAppBarLayoutCollapsed() {
        toolbar_layout.isTitleEnabled = false
        toolbar.title = "Foursquare App"
        toolbar_image.visibility = View.GONE
        val params = app_bar.getLayoutParams() as CoordinatorLayout.LayoutParams
        params.height = CoordinatorLayout.LayoutParams.WRAP_CONTENT
        app_bar.setLayoutParams(params)
        app_bar.setExpanded(false)
    }

    fun makeAppBarLayoutExpandable() {
        toolbar_layout.isTitleEnabled = true
        toolbar.title = ""
        toolbar_image.visibility = View.VISIBLE
        val params = app_bar.getLayoutParams() as CoordinatorLayout.LayoutParams
        params.height = resources.getDimensionPixelSize(R.dimen.app_bar_height)  // HEIGHT
        app_bar.layoutParams = params
        //enable scroll and expand
        val params1 = toolbar_layout.getLayoutParams() as AppBarLayout.LayoutParams
        params1.scrollFlags =
            AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
        app_bar.setExpanded(true)//expand
    }


}
